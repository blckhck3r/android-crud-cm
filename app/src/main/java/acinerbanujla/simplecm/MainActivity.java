package acinerbanujla.simplecm;

import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    DatabaseHelper dbConn;
    EditText firstName, lastName, contactEmail, contactNumber, searchfield;
    Button btnInsert, btnView, btnDelete, btnUpdate, exitBtn, searchBtn;
    Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        dbConn = new DatabaseHelper(this);
        super.onCreate(savedInstanceState);
//       supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        setView();

    }

    public void setView() {
        firstName = (EditText) findViewById(R.id.fname);
        lastName = (EditText) findViewById(R.id.lname);
        contactEmail = (EditText) findViewById(R.id.cmail);
        contactNumber = (EditText) findViewById(R.id.cnumber);
        btnInsert = (Button) findViewById(R.id.add_btn);
        btnView = (Button) findViewById(R.id.view_btn);
        searchfield = (EditText) findViewById(R.id.id_search);
        btnUpdate = (Button) findViewById(R.id.update_btn);
        searchBtn = (Button) findViewById(R.id.searchBtn);
        addData();
        viewData();
        delData();
        updateData();
        setSearchBtn();
//        exitBtn();

    }

    public void backpressPopupDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Hi, Do you want to close this application? ").setCancelable(false).setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int n) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.setTitle("Exit");
        alert.show();
    }

    @Override
    public void onBackPressed() {
        backpressPopupDialog();
        Toast.makeText(getApplicationContext(),
                "             Simple C R U D\nContact Management System\n" +
                        "   Created By. Acinerba Nujla\n" +
                        "             Aljun Abrenica", Toast.LENGTH_LONG).show();
    }

    public void setSearchBtn() {

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String sf = searchfield.getText().toString().trim();
                if (sf.length() == 0) {
                    Toast("Search field is empty");
                    return;
                }
                Cursor c = dbConn.CusrsorUpdate(sf);
                if (c.moveToNext()) {
                    firstName.setText(c.getString(1));
                    lastName.setText(c.getString(2));
                    contactEmail.setText(c.getString(3));
                    contactNumber.setText(c.getString(4));
                    Toast("Search found!");
                } else {
                    Toast("Data not found!");
                }
                dbConn.close();
            }
        });


    }

    public void MyCustomDialog() {
        Toast.makeText(getApplicationContext(), "\t" +
                        "          Simple Contact Manager\nSource Code: bitbucket.com/blckhck3r",
                Toast.LENGTH_LONG).show();
        myDialog = new Dialog(MainActivity.this);
        myDialog.setContentView(R.layout.custom_dialog);
        myDialog.setTitle("About");
        myDialog.show();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                MyCustomDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_menu, menu);
        return true;
    }


    public void popupUpdate() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Update Contact");
        builder.setMessage("Do you want to update this contact? ").setCancelable(false).setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String fn = firstName.getText().toString().trim();
                        String ln = lastName.getText().toString().trim();
                        String ce = contactEmail.getText().toString().trim();
                        String cn = contactNumber.getText().toString().trim();
                        String sf = searchfield.getText().toString().trim();
                        boolean x = dbConn.UpdateData(sf, new Contact(fn, ln, ce, cn));
                        if (x) {
                            Toast("Data successfully Updated");
                            clearText();
                        } else {
                            Toast("Data not Updated");
                        }
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbConn.close();
    }


    public void updateData() {
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (searchfield.getText().length() == 0) {
                    Toast("Search field is empty");
                    return;
                }
                if (firstName.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Fill in the firstname text field", Toast.LENGTH_SHORT).show();
                    return;
                } else if (lastName.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Fill in the lastname text field", Toast.LENGTH_SHORT).show();
                    return;
                } else if (contactEmail.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Fill in the email text field", Toast.LENGTH_SHORT).show();
                    return;
                } else if (contactNumber.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Fill in the contact Number text field", Toast.LENGTH_SHORT).show();
                    return;
                }

                popupUpdate();

            }
        });
    }

    public void Toast(String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
    }

    public void popupDelete(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete Contact");
        builder.setMessage("Do you want to delete this contact? ").setCancelable(false).setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Integer deleteRows = dbConn.deleteData(searchfield.getText().toString());
                        if (deleteRows > 0) {
                            Toast.makeText(getApplicationContext(), "Contact successfully deleted", Toast.LENGTH_LONG).show();
                            clearText();
                        } else {
                            Toast.makeText(getApplicationContext(), "Data not deleted", Toast.LENGTH_SHORT).show();
                        }

                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
    public void delData() {
        btnDelete = (Button) findViewById(R.id.del_btn);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (searchfield.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "            Fill in the search field,\nBefore proceeding this delete button.", Toast.LENGTH_SHORT).show();
                    return;
                }
                popupDelete();
            }

        });
    }

    public void addData() {
        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                validateInsert();
            }
        });
    }

    public void validateInsert() {
        final String fn = firstName.getText().toString().trim();
        final String ln = lastName.getText().toString().trim();
        final String ce = contactEmail.getText().toString().trim();
        final String cn = contactNumber.getText().toString().trim();
        if (firstName.getText().length() == 0) {
            Toast.makeText(getApplicationContext(), "Fill in the firstname text field", Toast.LENGTH_SHORT).show();
            return;
        } else if (lastName.getText().length() == 0) {
            Toast.makeText(getApplicationContext(), "Fill in the lastname text field", Toast.LENGTH_SHORT).show();
            return;
        } else if (contactEmail.getText().length() == 0) {
            Toast.makeText(getApplicationContext(), "Fill in the email text field", Toast.LENGTH_SHORT).show();
            return;
        } else if (contactNumber.getText().length() == 0) {
            Toast.makeText(getApplicationContext(), "Fill in the contact Number text field", Toast.LENGTH_SHORT).show();
            return;
        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Insert Contact");
            builder.setMessage("Do you want to insert this contact? ").setCancelable(false).setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            boolean isInserted = dbConn.insertData(new Contact(fn, ln, ce, cn));
                            if (isInserted == true) {
                                Toast.makeText(getApplicationContext(), "Contact Successfully Inserted", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Data not inserted", Toast.LENGTH_SHORT).show();
                            }
                            clearText();

                        }
                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();

        }
    }

    public void clearText() {
        searchfield.setText("");
        firstName.setText("");
        lastName.setText("");
        contactEmail.setText("");
        contactNumber.setText("");
        dbConn.close();
    }

    public void viewData() {
        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor rs = dbConn.getAllData();
                if (rs.getCount() == 0) {
                    //message
                    showMessage("Error", "Contact not found");
                    return;
                }
                StringBuffer buffer = new StringBuffer();
                while (rs.moveToNext()) {
                    buffer.append(" Id: " + rs.getString(0));
                    buffer.append("\n Firstname: " + rs.getString(1));
                    buffer.append("\n Lastname: " + rs.getString(2));
                    buffer.append("\n Email: " + rs.getString(3));
                    buffer.append("\n Number: " + rs.getString(4) + "\n\n");
                }
                showMessage("Contact List\n", buffer.toString());
            }
        });
    }//eof viewData

    public void showMessage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }
}
