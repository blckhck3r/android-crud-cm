package acinerbanujla.simplecm;

import android.provider.BaseColumns;

public class Contact {
    private int _id;
    private String fname;
    private String lname;
    private String email;
    private String contact;

    public Contact() {

    }

    public Contact(String fname, String lname, String email, String contact) {
        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.contact = contact;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public String getEmail() {
        return email;
    }

    public String getContact() {
        return contact;
    }

    public int getId() {
        return _id;
    }

    public static class ContactDB implements BaseColumns {
        public static final int VERSION = 1;
        public static final String DATABASENAME = "Contact.db";
        public static final String TABLENAME = "contact_tbl";
        public static final String FIRSTNAME = "fname";
        public static final String LASTNAME = "lname";
        public static final String EMAIL = "email";
        public static final String NUMBER = "contact";
    }


}
